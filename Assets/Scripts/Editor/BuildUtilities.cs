using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

namespace Editor
{
    public static class BuildUtilities
    {
        [MenuItem("Game/Run From Start")]
        public static void RunFromStart()
        {
            EditorSceneManager.OpenScene("Assets/Scenes/Bootstrap.unity");
            EditorApplication.isPlaying = true;
        }
    }
}