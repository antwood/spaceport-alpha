using System.Collections.Generic;
using Runtime.ScriptableObjects;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Editor
{
    [CustomEditor(typeof(CombatSequence))]
    public class CombatSequenceEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            var seq = target as CombatSequence;

            foreach (var step in seq.Steps)
            {
                DrawCombatStep(step);
            }

            if (GUILayout.Button("Add Step"))
            {
                var newSteps = new List<SequenceStep>(seq.Steps);
                newSteps.Add(new SequenceStep());

                seq.Steps = newSteps.ToArray();
            }
            
            //base.OnInspectorGUI();
        }

        private void DrawCombatStep(SequenceStep step)
        {
            EditorGUILayout.BeginHorizontal();

            step.Time = EditorGUILayout.FloatField(step.Time, GUILayout.Width(40f));
            step.Action = (CombatAction) EditorGUILayout.EnumPopup(step.Action);
            
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space(5f);

            foreach (var property in step.Properties)
            {
                EditorGUILayout.BeginHorizontal();
                property.Name = EditorGUILayout.TextField(property.Name);
                property.Value = EditorGUILayout.TextField(property.Value);
                if (GUILayout.Button("-", GUILayout.Width(30f)))
                {
                    
                }
                EditorGUILayout.EndHorizontal();
            }
            
            if (GUILayout.Button("Add Property"))
            {
                var newProperties = new List<SequenceStepProperty>(step.Properties) {new SequenceStepProperty()};
                step.Properties = newProperties.ToArray();
            }

            EditorGUILayout.Space(20f);
        }
    }
}