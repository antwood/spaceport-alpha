using Runtime.Systems;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    [CustomEditor(typeof(Ground))]
    [CanEditMultipleObjects]
    public class GroundEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Update Mappings"))
            {
                (target as Ground)?.UpdateMappings();
            }

            base.OnInspectorGUI();
        }
    }
}