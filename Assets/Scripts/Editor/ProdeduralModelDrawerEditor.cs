using Runtime.Systems;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    [CustomEditor(typeof(ProceduralModelDrawer))]
    public class ProdeduralModelDrawerEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Update Mesh"))
            {
                (target as ProceduralModelDrawer)?.GenerateModel();
            }
            base.OnInspectorGUI();
        }
    }
}