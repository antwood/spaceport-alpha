using System;
using UnityEngine;

namespace Multiplatform.Common
{
    public class Platform : ScriptableObject
    {
        public RuntimePlatform RuntimePlatform;
        
        public virtual void Initialise() { }

        public virtual string GetUserName() { return string.Empty; }
        
        public virtual void LoadData(string bufferName, Action<byte[]> dataLoadedCallback) { }
        public virtual void SaveData(string bufferName, byte[] data) {}
        
        public virtual void UnlockAchievement(string achievementId) { }
        
        public virtual void Update() {}
    }
}