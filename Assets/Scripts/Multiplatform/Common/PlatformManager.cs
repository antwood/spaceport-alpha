using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

namespace Multiplatform.Common
{
    public class PlatformManager : MonoBehaviour
    {
        public static PlatformManager Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void OnDestroy()
        {
            if (Instance == this)
            {
                Instance = null;
            }
        }

        [SerializeField] private Platform[] m_platformBindings;

        private Platform m_platform;
        
        private void Start()
        {
            m_platform = m_platformBindings.FirstOrDefault(b => b.RuntimePlatform == Application.platform);
            
            Assert.IsNotNull(m_platform, "Platform binding does not exist.");
            
            m_platform.Initialise();

            name = m_platform.name;
        }

        private void Update()
        {
            m_platform.Update();
        }

        public string GetUserName() 
        {
            return m_platform.GetUserName();
        }

        public void LoadData(string bufferName, Action<byte[]> dataLoadedCallback)
        {
            m_platform.LoadData(bufferName, dataLoadedCallback);
        }

        public void SaveData(string bufferName, byte[] data)
        {
            m_platform.SaveData(bufferName, data);
        }

        public void UnlockAchievement(string achievementId)
        {
            m_platform.UnlockAchievement(achievementId);
        }
    }
}