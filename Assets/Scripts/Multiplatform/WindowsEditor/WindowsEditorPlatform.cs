using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Multiplatform.Common;
using UnityEngine;

namespace Multiplatform.WindowsEditor
{
    [CreateAssetMenu(fileName = "Windows Editor Platform", menuName = "Platforms/Windows Editor")]
    public class WindowsEditorPlatform : Platform
    {
        private Dictionary<string, int> m_scoreTable = new Dictionary<string, int>
        {
            { "ant", 23979 },
            { "bob", 56759 },
            { "brenda", 57632 },
            { "john", 40235 },
        };
        
        private Dictionary<string, string> m_settings = new Dictionary<string, string>
        {
            { "volume", "0.5" },
            { "fullscreen", "true" },
            { "invert_y", "false" }
        };
        
        public override void Initialise()
        {
            base.Initialise();
        }

        public override string GetUserName()
        {
            return "ant";
        }

        public override void LoadData(string bufferName, Action<byte[]> dataLoadedCallback)
        {
            // Load data from dummy values rather than from disk.
            switch (bufferName)
            {
                case "high_scores":
                    using (var ms = new MemoryStream())
                    {
                        var bf = new BinaryFormatter();
                        bf.Serialize(ms, m_scoreTable);
                        
                        dataLoadedCallback?.Invoke(ms.GetBuffer());
                    }

                    break;
                
                case "settings":
                    using (var ms = new MemoryStream())
                    {
                        var bf = new BinaryFormatter();
                        bf.Serialize(ms, m_settings);
                        
                        dataLoadedCallback?.Invoke(ms.GetBuffer());
                    }

                    break;
            }
            
            dataLoadedCallback?.Invoke(null);
        }

        public override void SaveData(string bufferName, byte[] data)
        {
            base.SaveData(bufferName, data);
        }

        public override void UnlockAchievement(string achievementId)
        {
            base.UnlockAchievement(achievementId);
        }

        public override void Update()
        {
            base.Update();
        }
    }
}