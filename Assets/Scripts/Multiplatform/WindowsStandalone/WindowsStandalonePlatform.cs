using System;
using Multiplatform.Common;
using UnityEngine;

namespace Multiplatform.WindowsStandalone
{
    [CreateAssetMenu(fileName = "Windows Standalone Platform", menuName = "Platforms/Windows Standalone")]
    public class WindowsStandalonePlatform : Platform
    {
        public override void Initialise() { }

        public override string GetUserName()
        {
            return string.Empty;
        }

        public override void LoadData(string bufferName, Action<byte[]> dataLoadedCallback)
        {
            throw new NotImplementedException();
        }

        public override void SaveData(string bufferName, byte[] data)
        {
            throw new NotImplementedException();
        }

        public override void UnlockAchievement(string achievementId)
        {
            throw new NotImplementedException();
        }

        public override void Update() { }
    }
}