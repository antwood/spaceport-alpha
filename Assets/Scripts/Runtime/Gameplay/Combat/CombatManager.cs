using System.Collections.Generic;
using Runtime.ScriptableObjects;
using Runtime.Systems;
using Runtime.UI;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Runtime.Gameplay.Combat
{
    public class CombatManager : MonoBehaviour
    {
        public static CombatManager Instance { get; private set; }
        
        [SerializeField] private GameObject m_debrisPrefab;
        
        [SerializeField] private ScoreLabel m_scoreLabel;
        
        private int m_score;
        private int Score
        {
            get => m_score;
            set
            {
                m_score = value;
                m_scoreLabel.SetScore(m_score);
            }
        }

        private bool m_isCombatActive = true;
        public bool IsCombatActive => m_isCombatActive;
        
        private int m_debrisHash;
        
        private List<CombatTarget> m_combatTargets;
        private List<Projectile> m_playerProjectiles;
        private List<Projectile> m_enemyProjectiles;

        private PlayerShield m_playerShield;

        [SerializeField] private CombatSequence[] m_sequences;
        private int m_currentSequenceIndex;
        
        public List<CombatTarget> CombatTargets => m_combatTargets;

        private class AreaDamage
        {
            public Vector3 Position;
            public float Radius;
            public float Time;
        }

        private List<AreaDamage> m_currentAreaDamages;
        

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
                return;
            }
            
            m_combatTargets = new List<CombatTarget>();
            m_playerProjectiles = new List<Projectile>();
            m_enemyProjectiles = new List<Projectile>();

            m_currentAreaDamages = new List<AreaDamage>();
        }

        private void OnDestroy()
        {
            if (Instance == this)
            {
                Instance = null;
            }
        }

        private void Start()
        {
            m_debrisHash = ObjectPool.Instance.AddObject(m_debrisPrefab.GetComponent<IPoolable>());

            m_currentSequenceIndex = 0;
            m_sequences[m_currentSequenceIndex].Initialise();

            Score = 0;
        }

        public void StartCombat()
        {
            m_isCombatActive = true;
        }

        public void StopCombat()
        {
            m_isCombatActive = false;
        }
        
        public void Update()
        {
            if (!m_isCombatActive) return;
            if (GameManager.Instance.IsPaused) return;
            
            foreach (var projectile in m_playerProjectiles)
            {
                foreach (var target in m_combatTargets)
                {
                    if (target.Bounds.Contains(projectile.transform.position))
                    {
                        target.Hit();
                        projectile.Kill();
                        Score += 10;
                    }
                }
            }

            if (m_playerShield != null)
            {
                // Check player shield against projectiles
                foreach (var enemyProjectile in m_enemyProjectiles)
                {
                    if (enemyProjectile.MarkedForReturn) continue;

                    if (Vector3.Distance(enemyProjectile.transform.position, m_playerShield.transform.position) <
                        m_playerShield.Radius)
                    {
                        enemyProjectile.Kill();
                        m_playerShield.Hit();
                    }
                }

                // Check to make sure there is still a shield after checking against projectiles

                // Check player shield against enemies
                foreach (var target in m_combatTargets)
                {
                    if (target.MarkedForReturn) continue;

                    if (Vector3.Distance(target.transform.position, m_playerShield.transform.position) <
                        m_playerShield.Radius)
                    {
                        target.Hit();
                        m_playerShield.Hit();
                        Score += 10;
                    }
                }
                
            }

            foreach (var areaDamage in m_currentAreaDamages)
            {
                areaDamage.Time -= Time.deltaTime;
            }

            if (m_currentSequenceIndex < m_sequences.Length)
            {
                if (!m_sequences[m_currentSequenceIndex].UpdateSequence(m_combatTargets.Count))
                {
                    m_currentSequenceIndex++;
                    if (m_currentSequenceIndex >= m_sequences.Length)
                    {
                        GameManager.Instance.EndGame(true);
                    }
                    else
                    {
                        m_sequences[m_currentSequenceIndex].Initialise();
                    }
                }
            }
        }

        public void PerformAreaDamage(Vector3 position, float radius, int hitPoints)
        {
            m_currentAreaDamages.Add(new AreaDamage {Position = position, Radius = radius, Time = 1f} );
            
            foreach (var target in m_combatTargets)
            {
                if (Vector3.Distance(position, target.transform.position) < radius)
                {
                    target.Hit(hitPoints);
                }
            }
        }

        public void SpawnDebris(Vector3 position, float scale, int count)
        {
            for (int i = 0; i < count; i++)
            {
                var debris = ObjectPool.Instance.GetObject(m_debrisHash, position, Quaternion.identity);
                
                debris.Reset();
                debris.GetGameObject().transform.localScale = debris.GetGameObject().transform.localScale * scale;
                debris.GetGameObject().SetActive(true);
            }
        }
        
        public CombatTarget GetClosestCombatTarget(Transform compTransform)
        {
            var closestDistance = Mathf.Infinity;
            CombatTarget closest = null;
            
            foreach (var combatTarget in CombatTargets)
            {
                var distance = Vector3.Distance(compTransform.position, combatTarget.transform.position);
                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    closest = combatTarget;
                }
            }

            return closest;
        }

        public void AddPlayerShield(PlayerShield shield)
        {
            m_playerShield = shield;
        }

        public void RemovePlayerShield()
        {
            m_playerShield = null;
        }
        
        public void AddCombatTarget(CombatTarget target)
        {
            m_combatTargets.Add(target);
        }
        
        public void RemoveCombatTarget(CombatTarget target)
        {
            m_combatTargets.Remove(target);
        }

        public void AddPlayerProjectile(Projectile projectile)
        {
            m_playerProjectiles.Add(projectile);
        }

        public void RemovePlayerProjectile(Projectile projectile)
        {
            m_playerProjectiles.Remove(projectile);
        }

        public void AddEnemyProjectile(Projectile projectile)
        {
            m_enemyProjectiles.Add(projectile);
        }

        public void RemoveEnemyProjectile(Projectile projectile)
        {
            m_enemyProjectiles.Remove(projectile);
        }

        private void OnDrawGizmos()
        {
            if (m_currentAreaDamages == null) return;
            foreach (var areaDamage in m_currentAreaDamages)
            {
                if (areaDamage.Time > 0f) Gizmos.DrawSphere(areaDamage.Position, areaDamage.Radius);
            }
        }
    }
}