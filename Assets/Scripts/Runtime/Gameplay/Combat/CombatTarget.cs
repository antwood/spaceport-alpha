using System.Collections.Generic;
using Runtime.Systems;
using UnityEngine;

namespace Runtime.Gameplay.Combat
{
    public class CombatTarget : MonoBehaviour, IPoolable
    {
        [SerializeField] private int m_initialHitPoints = 6;
        [SerializeField] private int m_destructionHitPoints = 10;
        [SerializeField] private float m_destructionRadius = 1f;
        [SerializeField] private int m_debrisCount = 3;
        [SerializeField] private float m_debrisScale = 1f;
        
        private List<ITargetEffect> m_effects;

        private int m_currentHitPoints;
        private bool m_markedForReturn;

        public Bounds Bounds => new Bounds(transform.position, new Vector3(1f, 1f, 1f));
        public bool MarkedForReturn => m_markedForReturn;

        private void Awake()
        {
            GetEffects();
        }

        private void GetEffects()
        {
            if (m_effects == null)
            {
                m_effects = new List<ITargetEffect>();
            }
            else
            {
                m_effects.Clear();
            }
            
            var effects = GetComponentsInChildren<ITargetEffect>();
            if (effects.Length > 0) m_effects.AddRange(effects);
        }

        public string GetTag()
        {
            return name;
        }

        public GameObject GetGameObject()
        {
            return gameObject;
        }

        public void Reset()
        {
            m_markedForReturn = false;
            m_currentHitPoints = m_initialHitPoints;
            CombatManager.Instance.AddCombatTarget(this);

            foreach (var targetEffect in m_effects)
            {
                targetEffect.Reset(m_initialHitPoints);
            }
        }

        public void LateUpdate()
        {
            if (m_markedForReturn)
            {
                CombatManager.Instance.RemoveCombatTarget(this);
                ObjectPool.Instance.ReturnObject(this);
            }
        }

        public void Hit(int hitPoints = 1)
        {
            if (m_markedForReturn) return;
            
            m_currentHitPoints -= hitPoints;
            
            foreach (var targetEffect in m_effects)
            {
                targetEffect.Hit(m_initialHitPoints - m_currentHitPoints);
            }

            if (m_currentHitPoints <= 0)
            {
                m_markedForReturn = true;
                CombatManager.Instance.PerformAreaDamage(transform.position, m_destructionRadius, m_destructionHitPoints);
                CombatManager.Instance.SpawnDebris(transform.position, m_debrisScale, m_debrisCount);
            }
        }
    }
}