using System;
using Runtime.Systems;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Runtime.Gameplay.Combat
{
    public class Debris : MonoBehaviour, IPoolable
    {
        [SerializeField] private float m_rotationSpeed = 10f;
        [SerializeField] private float m_moveSpeed = 10f;
        [SerializeField] private float m_lifeTime = 3f;

        private float m_currentTime;
        private Vector3 m_direction;

        private float m_currentMoveSpeed;

        public string GetTag()
        {
            return name;
        }

        public GameObject GetGameObject()
        {
            return gameObject;
        }

        public void Reset()
        {
            m_currentTime = m_lifeTime;
            m_direction = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0f).normalized;
            transform.localScale = Vector3.one;
            m_currentMoveSpeed = m_moveSpeed;
        }

        private void Update()
        {
            if (GameManager.Instance.IsPaused) return;
            
            if (m_currentTime > 0f)
            {
                var rotation = transform.rotation;
                rotation.eulerAngles += Vector3.forward * Time.deltaTime * m_rotationSpeed;
                transform.rotation = rotation;
            
                var position = transform.position;
                position += Time.deltaTime * m_direction * m_currentMoveSpeed;
                transform.position = position;

                var scale = transform.localScale;
                scale *= 0.999f;
                transform.localScale = scale;

                m_currentMoveSpeed *= 0.999f;

                m_currentTime -= Time.deltaTime;
            }
            else
            {
                ObjectPool.Instance.ReturnObject(this);
            }
        }
    }
}