namespace Runtime.Gameplay.Combat
{
    public interface ITargetEffect
    {
        void Hit(int accumulatedHitPoints);
        void Reset(int totalHitPoints);
    }
}