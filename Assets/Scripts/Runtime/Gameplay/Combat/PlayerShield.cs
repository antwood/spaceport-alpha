using System;
using System.Collections;
using Runtime.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Runtime.Gameplay.Combat
{
    public class PlayerShield : MonoBehaviour
    {
        [SerializeField] private float m_radius = 2f;

        [SerializeField] private int m_initialPoints = 5;

        [SerializeField] private ShieldIndicator m_indicator;
        [SerializeField] private Material m_material;

        private float m_indicatorTargetValue = 1f;

        private int m_currentPoints;

        private PlayerController m_controller;
        private bool m_markedForRemoval;
        private bool m_hitThisFrame;
        private float m_currentIndicatorValue;
        private float m_indicatorValueVelocity;

        public float Radius => m_radius;

        private void Awake()
        {
            m_controller = GetComponent<PlayerController>();
        }

        private void Start()
        {
            CombatManager.Instance.AddPlayerShield(this);

            m_currentPoints = m_initialPoints;
            m_markedForRemoval = false;

            m_indicatorTargetValue = 1f;
            m_currentIndicatorValue = 0f;

            m_indicator.Total = m_initialPoints;
            
            m_material.SetColor("_EmissionColor", new Color(0f, 0f, 30f));
        }

        private void Update()
        {
            m_hitThisFrame = false;
            
            if (m_markedForRemoval)
            {
                CombatManager.Instance.RemovePlayerShield();
            }

            m_currentIndicatorValue = Mathf.SmoothDamp(m_currentIndicatorValue, m_indicatorTargetValue,
                ref m_indicatorValueVelocity, 0.2f);
            
            m_indicator.Value = m_currentIndicatorValue;
        }

        public void Hit(int points = 1)
        {
            if (m_markedForRemoval) return;
            if (m_hitThisFrame) return;

            m_hitThisFrame = true;
            m_currentPoints -= points;

            m_indicatorTargetValue = (float) m_currentPoints / m_initialPoints;

            if (m_currentPoints <= 0)
            {
                m_markedForRemoval = true;
                m_controller.Kill();
            }
            else
            {
                CancelInvoke();
                StartCoroutine(Flash());
            }
        }
        
        private void OnDrawGizmosSelected()
        {
            Gizmos.DrawWireSphere(transform.position, m_radius);
        }
        
        private IEnumerator Flash() 
        {
            for (var i = 0; i < 10; i++)
            {
                m_material.SetColor("_EmissionColor", i % 2 == 0 ? new Color(30f, 0f, 0f) : new Color(0f, 0f, 30f));

                yield return new WaitForSeconds(0.25f);
            }
        }
    }
}