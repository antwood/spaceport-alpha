using Runtime.Systems;
using UnityEngine;

namespace Runtime.Gameplay.Combat
{
    public class Projectile : MonoBehaviour, IPoolable
    {
        [SerializeField] private float m_speed = 50f;
        [SerializeField] private bool m_isPlayerProjectile;
        
        private Vector3 m_direction;
        private bool m_markForReturn;

        public bool MarkedForReturn => m_markForReturn;
        
        public void Kill()
        {
            m_markForReturn = true;
        }

        private void ReturnToPool()
        {
            if (m_isPlayerProjectile)
            {
                CombatManager.Instance.RemovePlayerProjectile(this);
            }
            else
            {
                CombatManager.Instance.RemoveEnemyProjectile(this);
            }

            ObjectPool.Instance.ReturnObject(this);
        }

        private void Update()
        {
            if (GameManager.Instance.IsPaused) return;
            
            transform.position += m_direction * Time.deltaTime * m_speed;

            if (!GameManager.Instance.InBounds(transform.position))
            {
                Kill();
            }
        }

        private void LateUpdate()
        {
            if (m_markForReturn) ReturnToPool();
        }

        public string GetTag()
        {
            return gameObject.name;
        }

        public GameObject GetGameObject()
        {
            return gameObject;
        }

        public void Reset()
        {
            if (m_isPlayerProjectile)
            {
                CombatManager.Instance.AddPlayerProjectile(this);
            }
            else
            {
                CombatManager.Instance.AddEnemyProjectile(this);
            }

            m_markForReturn = false;
            var theta = -transform.rotation.eulerAngles.z * Mathf.Deg2Rad;
            m_direction = new Vector3(Mathf.Sin(theta), Mathf.Cos(theta), 0f);
        }
    }
}