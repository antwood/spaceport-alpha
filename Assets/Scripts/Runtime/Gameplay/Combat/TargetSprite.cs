using UnityEngine;

namespace Runtime.Gameplay.Combat
{
    public class TargetSprite : MonoBehaviour, ITargetEffect
    {
        private SpriteRenderer m_sprite;
        private int m_totalHitPoints;

        private void Awake()
        {
            m_sprite = GetComponent<SpriteRenderer>();
        }

        public void Hit(int accumulatedHitPoints)
        {
            m_sprite.color = Color.Lerp(Color.white, Color.red, accumulatedHitPoints / (float)m_totalHitPoints);
        }

        public void Reset(int totalHitPoints)
        {
            m_totalHitPoints = totalHitPoints;
            if (m_sprite != null) m_sprite.color = Color.white;
        }
    }
}