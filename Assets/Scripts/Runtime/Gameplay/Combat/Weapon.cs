using Runtime.Systems;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Runtime.Gameplay.Combat
{
    public class Weapon : MonoBehaviour
    {
        private PlayerInput m_input;

        private InputAction m_fireAction;

        [SerializeField] private float m_coolDown = 0.25f;
        [SerializeField] private GameObject m_projectilePrefab;

        private int m_projectileHash;
        
        private float m_currentCooldown = 0f;

        [SerializeField] private bool m_isAutoFire;
        
        private void Awake()
        {
            m_input = GetComponentInParent<PlayerInput>();

            m_isAutoFire = m_input == null;
        }

        private void Start()
        {
            if (!m_isAutoFire) m_fireAction = m_input.actions["Fire"];
            
            m_projectileHash = ObjectPool.Instance.AddObject(m_projectilePrefab.GetComponent<IPoolable>());
        }

        private void Update()
        {
            if (GameManager.Instance.IsPaused) return;
            
            if (!m_isAutoFire)
            {
                if (m_fireAction.IsPressed() && m_currentCooldown <= 0f)
                {
                    Fire();
                }
            }
            else
            {
                if (m_currentCooldown <= 0f)
                {
                    Fire();
                }
            }

            m_currentCooldown -= Time.deltaTime;
        }

        private void Fire()
        {
            var transform1 = transform;
            var projectile = ObjectPool.Instance.GetObject(m_projectileHash, transform1.position, transform1.parent.rotation);

            projectile.Reset();
            projectile.GetGameObject().SetActive(true);

            m_currentCooldown = m_coolDown;
        }
    }
}