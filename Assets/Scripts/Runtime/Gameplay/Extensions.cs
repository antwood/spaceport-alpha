using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace Runtime.Gameplay
{
    public static class Extensions
    {
        public static Vector3 Vector3(this Vector2 vector)
        {
            return new Vector3(vector.x, vector.y, 0f);
        }

        public static Vector2 Vector2(this Vector3 vector)
        {
            return new Vector2(vector.x, vector.y);
        }

        public static Vector3 VectorScale(this Vector3 vector, Vector3 scale)
        {
            return new Vector3(vector.x * scale.x, vector.y * scale.y, vector.z * scale.z);
        }
        
        public static Vector3 VectorOffset(this Vector3 vector, Vector3 offset)
        {
            return new Vector3(vector.x + offset.x, vector.y + offset.y, vector.z + offset.z);
        }

        public static Vector3 ToVector3(this string input)
        {
            var clean = input.Trim("()".ToCharArray());
            var parts = clean.Split(',');

            Assert.AreEqual(parts.Length, 3);

            return new Vector3(
                float.Parse(parts[0].Replace(" ", "")),
                float.Parse(parts[1].Replace(" ", "")),
                float.Parse(parts[2].Replace(" ", "")));
        }
    }
}