using System.Collections.Generic;
using Runtime.Gameplay.Movement;
using Runtime.ScriptableObjects;
using Runtime.UI;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Runtime.Gameplay
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance { get; private set; }

        [SerializeField ]private PlayerInput m_input;
        
        private List<Attractor> m_attractors;

        [SerializeField] private MenuController m_menu;
        [SerializeField] private GameObject m_gameUI;
        [SerializeField] private DialoguePanel m_dialogue;
        
        private bool m_isPaused = false;
        public bool IsPaused => m_isPaused;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
                return;
            }
            
            m_attractors = new List<Attractor>();
        }

        private void OnDestroy()
        {
            if (Instance == this)
            {
                Instance = null;
            }
        }

        [SerializeField] private Bounds m_gameplayBounds;

        public Bounds GameplayBounds => m_gameplayBounds;

        private Vector3 m_attractionPoint;
        public Vector3 AttractionPoint => m_attractionPoint;

        private void Update()
        {
            if (IsPaused)
            {
                if (m_dialogue.gameObject.activeSelf)
                {
                    if (m_input.actions["Submit"].WasPressedThisFrame())
                    {
                        m_dialogue.AdvanceDialogue();
                    }
                }
                else
                {
                    if (m_input.actions["Menu"].WasPressedThisFrame())
                    {
                        UnPause();
                    }
                }
            }
            else
            {
                if (m_input.actions["Menu"].WasPressedThisFrame())
                {
                    Pause();
                    return;
                }
                
                m_attractionPoint = Vector3.zero;
            
                foreach (var attractor in m_attractors)
                {
                    m_attractionPoint += attractor.transform.position * attractor.Weight;
                }
            }
        }

        public void AddAttractor(Attractor newAttractor)
        {
            m_attractors.Add(newAttractor);
        }

        public void RemoveAttractor(Attractor attractor)
        {
            m_attractors.Remove(attractor);
        }

        public bool InBounds(Vector3 position)
        {
            return m_gameplayBounds.Contains(position);
        }

        public void Pause()
        {
            m_isPaused = true;
            m_menu.ShowMenu("Pause Menu");
            m_gameUI.SetActive(false);
        }

        public void UnPause()
        {
            m_isPaused = false;
            m_menu.ShowMenu("Clear");
            m_gameUI.SetActive(true);
        }

        public void EndGame(bool didWin)
        {
            m_menu.ShowMenu(didWin ? "Game Over Win" : "Game Over Lose");
            m_gameUI.SetActive(false);
        }

        public void StartDialogue(string dialogueName)
        {
            m_isPaused = true;
            m_dialogue.StartDialogue(dialogueName, () => m_isPaused = false);
        }
        
#if UNITY_EDITOR

        private void OnDrawGizmos()
        {
            Gizmos.DrawWireCube(m_gameplayBounds.center, m_gameplayBounds.size);
        }

#endif
    }
}