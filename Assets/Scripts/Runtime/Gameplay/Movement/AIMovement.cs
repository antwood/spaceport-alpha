using Runtime.Gameplay.Combat;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Runtime.Gameplay.Movement
{
    public class AIMovement : MonoBehaviour, ITargetEffect
    {
        [SerializeField] private float m_moveSpeed = 1f;
        [SerializeField] private float m_turnSpeed = 10f;
        [SerializeField] private float m_headingUpdateSpeed = 1f;
        
        private Vector3 m_direction;
        private Vector3 m_targetDirection;
        private Vector3 m_directionVelocity;
        
        private Quaternion m_targetRotation;

        private void Update()
        {
            if (GameManager.Instance.IsPaused) return;
            
            m_direction = Vector3.SmoothDamp(m_direction, m_targetDirection, ref m_directionVelocity, 1f);
            
            var position = transform.position;
            
            position += m_direction * Time.deltaTime * m_moveSpeed;

            if (!GameManager.Instance.InBounds(position))
            {
                var gameplayBounds = GameManager.Instance.GameplayBounds;
                if (position.x < gameplayBounds.min.x || position.x > gameplayBounds.max.x)
                {
                    position.x = -position.x;
                }

                if (position.y < gameplayBounds.min.y || position.y > gameplayBounds.max.y)
                {
                    position.y = -position.y;
                }
            }

            transform.position = position;
            
            var theta = Mathf.Atan2(m_direction.x, m_direction.y) * Mathf.Rad2Deg;
            m_targetRotation = Quaternion.Euler(0f, 0f, -theta);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, m_targetRotation, m_turnSpeed * Time.deltaTime);
        }
        
        private void UpdateDirection()
        {
            //m_theta += Random.Range(-90f, 90f) * Mathf.Deg2Rad;
            //m_targetDirection = new Vector3(Mathf.Sin(m_theta), Mathf.Cos(m_theta), 0f);

            m_targetDirection = Vector3.Normalize(GameManager.Instance.AttractionPoint - transform.position);
            
            Invoke(nameof(UpdateDirection), m_headingUpdateSpeed);
        }
        
        private void PlaceRandom()
        {
            var bounds = GameManager.Instance.GameplayBounds;
            transform.position = new Vector3(
                Random.Range(-1f, 1f) < 0f ? bounds.min.x : bounds.max.x, 
                //Random.Range(bounds.min.x, bounds.max.x),
                Random.Range(bounds.min.y, bounds.max.y),// < 0f ? bounds.min.y : bounds.max.y, 
                0f);
        }

        public void Hit(int accumulatedHitPoints) { }

        public void Reset(int totalHitPoints)
        {
            enabled = false;
            
            UpdateDirection();
            
            PlaceRandom();
        }
    }
}