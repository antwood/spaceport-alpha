using System;
using Runtime.Gameplay.Combat;
using UnityEngine;

namespace Runtime.Gameplay.Movement
{
    public class AnimatedMovement : MonoBehaviour, ITargetEffect
    {
        private Animation m_animator;

        private void Awake()
        {
            m_animator = GetComponent<Animation>();
        }

        public void Hit(int accumulatedHitPoints) { }

        public void Reset(int totalHitPoints)
        {
            enabled = false;
        }

        public void SetAnimation(string animationClipName)
        {
            
        }
    }
}