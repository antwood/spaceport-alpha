using UnityEngine;

namespace Runtime.Gameplay.Movement
{
    public class Attractor : MonoBehaviour
    {
        [SerializeField] private float m_weight;

        public float Weight => m_weight;

        private void Start()
        {
            GameManager.Instance.AddAttractor(this);
        }
    }
}