using System;
using Runtime.Gameplay.Combat;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Runtime.Gameplay.Movement
{
    public class ForwardMovement : MonoBehaviour, ITargetEffect
    {
        [SerializeField] private float m_speed = 10f;
        
        private void Update()
        {
            var position = transform.position;
            position += Time.deltaTime * m_speed * transform.up;
            
            if (!GameManager.Instance.InBounds(position))
            {
                var gameplayBounds = GameManager.Instance.GameplayBounds;
                if (position.x < gameplayBounds.min.x || position.x > gameplayBounds.max.x)
                {
                    position.x = -position.x;
                }

                if (position.y < gameplayBounds.min.y || position.y > gameplayBounds.max.y)
                {
                    position.y = -position.y;
                }
            }
            
            transform.position = position;
        }
        
        private void PlaceRandom()
        {
            var bounds = GameManager.Instance.GameplayBounds;
            transform.position = new Vector3(
                Random.Range(-1f, 1f) < 0f ? bounds.min.x : bounds.max.x, 
                //Random.Range(bounds.min.x, bounds.max.x),
                Random.Range(bounds.min.y, bounds.max.y),// < 0f ? bounds.min.y : bounds.max.y, 
                0f);
        }

        public void Hit(int accumulatedHitPoints) { }

        public void Reset(int totalHitPoints)
        {
            enabled = false;
            
            transform.rotation = Quaternion.Euler(0f, 0f, Random.Range(0f, 359f));
            PlaceRandom();
        }
    }
}