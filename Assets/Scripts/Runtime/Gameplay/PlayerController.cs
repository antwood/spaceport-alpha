using Runtime.Gameplay.Combat;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Runtime.Gameplay
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private float m_moveSpeed = 10f;
        [SerializeField] private float m_turnSpeed = 100f;

        [SerializeField] private Camera m_gameplayCamera;

        private float m_currentAcceleration;
        private float m_accelerationVelocity;
        
        private Quaternion m_targetRotation;
        private CombatTarget m_currentTarget;
        
        private PlayerInput m_input;

        private void Awake()
        {
            m_input = GetComponent<PlayerInput>();
        }

        public void Update()
        {
            if (GameManager.Instance.IsPaused) return;

            var moveValue = m_input.actions["Move"].ReadValue<Vector2>();
            m_currentAcceleration = Mathf.SmoothDamp(m_currentAcceleration,
                m_moveSpeed * (moveValue.magnitude == 0f ? 0f : 1f), ref m_accelerationVelocity, 0.1f);
            
            var position = transform.position;
            position += moveValue.Vector3() * Time.deltaTime * m_currentAcceleration;

            if (GameManager.Instance.InBounds(position))
            {
                transform.position = position;
            }
            
            if (m_input.currentControlScheme.Equals("Gamepad"))
            {
                var lookVector = m_input.actions["Look"].ReadValue<Vector2>();
                
                if (m_input.actions["Lock"].IsPressed())
                {
                    m_currentTarget = CombatManager.Instance.GetClosestCombatTarget(transform);

                    if (m_currentTarget != null)
                    {
                        var direction = Vector3.Normalize(m_currentTarget.transform.position - transform.position);
                        var theta = Mathf.Atan2(direction.x, direction.y) * Mathf.Rad2Deg;
                        m_targetRotation = Quaternion.Euler(0f, 0f, -theta);
                    }
                }
                else if (lookVector.magnitude > 0.5f)
                {
                    var theta = Mathf.Atan2(lookVector.x, lookVector.y) * Mathf.Rad2Deg;
                    m_targetRotation = Quaternion.Euler(0f, 0f, -theta);
                }
            }
            else
            {
                var mousePos = m_input.actions["MousePosition"].ReadValue<Vector2>();
                var playerPos = m_gameplayCamera.WorldToScreenPoint(transform.position).Vector2();
                var direction = mousePos - playerPos;
                var theta = Mathf.Atan2(direction.x, direction.y) * Mathf.Rad2Deg;
                m_targetRotation = Quaternion.Euler(0f, 0f, -theta);
            }
            
            transform.rotation = Quaternion.RotateTowards(transform.rotation, m_targetRotation, m_turnSpeed * Time.deltaTime);
        }

        public void Kill()
        {
            CombatManager.Instance.SpawnDebris(transform.position, 1f, 10);

            this.gameObject.SetActive(false);
            
            GameManager.Instance.EndGame(false);
        }
    }
}
