using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Runtime.Gameplay;
using Runtime.Gameplay.Movement;
using Runtime.Systems;
using Runtime.UI;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering;

namespace Runtime.ScriptableObjects
{
    [CreateAssetMenu(fileName = "CombatSequence", menuName = "Sequence/Combat", order = 0)]
    public class CombatSequence : ScriptableObject
    {
        public SequenceStep[] Steps;
        public bool Loop;

        private bool m_waitingForClear = false;
        
        [SerializeField] private GameObject m_minePrefab;
        private int m_mineHash;
        
        [SerializeField] private GameObject m_fighterPrefab;
        private int m_fighterHash;
        
        [SerializeField] private GameObject m_eliteFighterPrefab;
        private int m_eliteFighterHash;

        private float m_sequenceTime = 0f;

        private Stack<SequenceStep> m_sequence;
        private SequenceStep m_currentStep;
        
        // Initialisation
        public void Initialise()
        {
            m_mineHash = ObjectPool.Instance.AddObject(m_minePrefab.GetComponent<IPoolable>());
            m_fighterHash = ObjectPool.Instance.AddObject(m_fighterPrefab.GetComponent<IPoolable>());
            m_eliteFighterHash = ObjectPool.Instance.AddObject(m_eliteFighterPrefab.GetComponent<IPoolable>());

            ResetSequence();
        }

        private void ResetSequence()
        {
            m_sequence = new Stack<SequenceStep>();
            for (int i = Steps.Length - 1; i >= 0; i--)
            {
                m_sequence.Push(Steps[i]);
            }

            m_sequenceTime = 0f;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="activeTargets"></param>
        /// <returns>false if the sequence ended</returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public bool UpdateSequence(int activeTargets)
        {
            if (m_waitingForClear && activeTargets <= 0)
            {
                m_waitingForClear = false;
            }

            if (m_waitingForClear) return true;

            m_sequenceTime += Time.deltaTime;

            if (m_sequence.Count <= 0)
            {
                if (Steps == null || Steps.Length <= 0) return false;

                if (Loop)
                {
                    ResetSequence();
                }
                
                return false;
            }
            
            if (m_sequence.Peek().Time < m_sequenceTime)
            {
                m_currentStep = m_sequence.Pop();

                switch (m_currentStep.Action)
                {
                    case CombatAction.SpawnMine:
                        SpawnMine();
                        break;
                    
                    case CombatAction.SpawnFighter:
                        SpawnFighter();
                        break;
                    
                    case CombatAction.SpawnEliteFighter:
                        SpawnEliteFighter();
                        break;
                    
                    case CombatAction.WaitForClear:
                        WaitForClear();
                        break;
                    
                    case CombatAction.PlayDialogue:
                        PlayDialogue();
                        break;
                    
                    case CombatAction.WaitForTime:
                        break;
                    
                    case CombatAction.ShowMessage:
                        ShowMessage();
                        break;
                    
                    case CombatAction.MissionComplete:
                        MissionComplete();
                        break;
                    
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                m_currentStep = null;
            }

            return true;
        }

        private void MissionComplete()
        {
            // Show the mission complete message and end the game.
            var message = m_currentStep.GetProperty("message");
            if (message != null)
            {
                MessageLabel.Instance.SetMessage(message, 3f);
            }
        }

        private void ShowMessage()
        {
            var message = m_currentStep.GetProperty("message");
            var duration = m_currentStep.GetProperty("duration");
            if (message != null)
            {
                MessageLabel.Instance.SetMessage(message, float.TryParse(duration, out var durationFloat) ? durationFloat : 3f);
            }
        }

        // Combat Actions
        public void WaitForClear()
        {
            m_waitingForClear = true;
        }

        private void PlayDialogue()
        {
            var property = m_currentStep.Properties.FirstOrDefault(p => p.Name.Equals("dialogue"));

            if (property == null)
            {
                Debug.LogError($"Dialogue not found");
                return;
            }
            
            GameManager.Instance.StartDialogue(property.Value);
        }
        
        private void SpawnMine()
        {
            var mine = ObjectPool.Instance.GetObject(m_mineHash);
            mine.Reset();
            mine.GetGameObject().SetActive(true);

            ApplyProperties(mine);
        }
        
        private void SpawnFighter()
        {
            var fighter = ObjectPool.Instance.GetObject(m_fighterHash);
            fighter.Reset();
            fighter.GetGameObject().SetActive(true);
            
            ApplyProperties(fighter);
        }

        private void ApplyProperties(IPoolable poolable)
        {
            var animationProperty = m_currentStep?.Properties?.FirstOrDefault(p => p.Name.Equals("animation"));
            if (animationProperty != null)
            {
                var animation = poolable.GetGameObject().GetComponent<Animation>();
                animation.enabled = true;
                animation.Play(animationProperty.Value);
                return;
            }

            var typeProperty = m_currentStep?.Properties?.FirstOrDefault(p => p.Name.Equals("movement"));
            if (typeProperty != null)
            {
                var value = typeProperty.Value;
                switch (value)
                {
                    case "ai":
                        poolable.GetGameObject().GetComponent<AIMovement>().enabled = true;
                        break;
                    case "forward":
                        poolable.GetGameObject().GetComponent<ForwardMovement>().enabled = true;
                        break;
                }
            }
            else
            {
                poolable.GetGameObject().GetComponent<AIMovement>().enabled = true;
            }

            var positionProperty = m_currentStep?.Properties?.FirstOrDefault(p => p.Name.Equals("position"));
            if (positionProperty != null)
            {
                var value = positionProperty.Value;
                value = value.Replace("r", GameManager.Instance.GameplayBounds.max.x.ToString());
                value = value.Replace("l", GameManager.Instance.GameplayBounds.min.x.ToString());

                poolable.GetGameObject().transform.position = value.ToVector3();
            }

            var rotationProperty = m_currentStep?.Properties?.FirstOrDefault(p => p.Name.Equals("rotation"));
            if (rotationProperty != null)
            {
                poolable.GetGameObject().transform.rotation =
                    Quaternion.Euler(0f, 0f, float.Parse(rotationProperty.Value));
            }
        }

        private void SpawnEliteFighter()
        {
            var fighter = ObjectPool.Instance.GetObject(m_eliteFighterHash);
            fighter.Reset();
            fighter.GetGameObject().SetActive(true);
            
            ApplyProperties(fighter);
        }

        private void OnValidate()
        {
            /*var sortList = new SortedSet<SequenceStep>(new ByTime());

            foreach (var step in Steps)
            {
                sortList.Add(step);
            }
            
            Steps = sortList.ToArray();*/
        }
    }

    [System.Serializable]
    public class SequenceStep
    {
        public float Time;
        public CombatAction Action;
        public SequenceStepProperty[] Properties;

        public string GetProperty(string propertyName)
        {
            var property = Properties.FirstOrDefault(p => p.Name.Equals(propertyName));
            return property?.Value;
        }
    }

    [System.Serializable]
    public class SequenceStepProperty
    {
        public string Name;
        public string Value;
    }

    [System.Serializable]
    public enum CombatAction
    {
        SpawnMine,
        SpawnFighter,
        SpawnEliteFighter,
        WaitForClear,
        PlayDialogue,
        WaitForTime,
        ShowMessage,
        MissionComplete
    }

    public class ByTime : IComparer<SequenceStep>
    {
        public int Compare(SequenceStep x, SequenceStep y)
        {
            if (ReferenceEquals(x, y)) return 0;
            if (ReferenceEquals(null, y)) return 1;
            if (ReferenceEquals(null, x)) return -1;
            return x.Time.CompareTo(y.Time);
        }
    }
}