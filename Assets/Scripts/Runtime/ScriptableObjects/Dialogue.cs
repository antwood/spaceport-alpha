using UnityEngine;

namespace Runtime.ScriptableObjects
{
    [CreateAssetMenu(fileName = "NewDialogue", menuName = "Dialogue", order = 0)]
    public class Dialogue : ScriptableObject
    {
        public DialogueElement[] DialoguesElements;
    }

    [System.Serializable]
    public class DialogueElement
    {
        public string Name;
        public string Dialogue;
    }
}