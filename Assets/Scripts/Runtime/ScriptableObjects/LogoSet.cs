using UnityEngine;

namespace Runtime.ScriptableObjects
{
    [CreateAssetMenu(fileName = "New LogoSet", menuName = "LogoSet")]
    public class LogoSet : ScriptableObject
    {
        public float DisplayTime = 1f;
        public float FadeTime = 1f;
        public Sprite[] Logos;
    }
}