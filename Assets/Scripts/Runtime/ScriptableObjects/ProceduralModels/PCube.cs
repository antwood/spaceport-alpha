using System;
using System.Collections.Generic;
using UnityEngine;

namespace Runtime.ScriptableObjects.ProceduralModels
{
    [CreateAssetMenu(fileName = "ProceduralCube", menuName = "Procedural/Cube", order = 0)]
    public class PCube : ProceduralModel
    {
        [SerializeField] private float m_sideLength = 1f;
        [SerializeField] private CubeSides m_sides;
        
        public CubeSides Sides => m_sides;
        
        public override Mesh GenerateMesh()
        {
            var verts = new List<Vector3>();
            var tris = new List<int>();
            var uvs = new List<Vector2>();

            var sideLength = m_sideLength * 0.5f;
            var index = 0;
            
            // Front
            if ((m_sides & CubeSides.Front) != 0)
            {
                verts.Add(new Vector3(-sideLength, sideLength, -sideLength)); // 0
                uvs.Add(new Vector2(0f, 0f));

                verts.Add(new Vector3(sideLength, sideLength, -sideLength)); // 1
                uvs.Add(new Vector2(1f, 0f));

                verts.Add(new Vector3(-sideLength, -sideLength, -sideLength)); // 2 
                uvs.Add(new Vector2(0f, 1f));

                verts.Add(new Vector3(sideLength, -sideLength, -sideLength)); // 3
                uvs.Add(new Vector2(1f, 1f));

                tris.Add(index);
                tris.Add(index + 1);
                tris.Add(index + 2);
                tris.Add(index + 2);
                tris.Add(index + 1);
                tris.Add(index + 3);
                index += 4;
            }

            // Rear

            if ((m_sides & CubeSides.Rear) != 0)
            {
                verts.Add(new Vector3(sideLength, sideLength, sideLength)); // 4
                uvs.Add(new Vector2(0f, 0f));

                verts.Add(new Vector3(-sideLength, sideLength, sideLength)); // 5
                uvs.Add(new Vector2(1f, 0f));

                verts.Add(new Vector3(sideLength, -sideLength, sideLength)); // 6
                uvs.Add(new Vector2(0f, 1f));

                verts.Add(new Vector3(-sideLength, -sideLength, sideLength)); // 7
                uvs.Add(new Vector2(1f, 1f));

                tris.Add(index);
                tris.Add(index + 1);
                tris.Add(index + 2);
                tris.Add(index + 2);
                tris.Add(index + 1);
                tris.Add(index + 3);
                index += 4;
            }

            // Left
            if ((m_sides & CubeSides.Left) != 0)
            {
                verts.Add(new Vector3(-sideLength, sideLength, sideLength)); // 8
                uvs.Add(new Vector2(0f, 0f));

                verts.Add(new Vector3(-sideLength, sideLength, -sideLength)); // 9
                uvs.Add(new Vector2(1f, 0f));

                verts.Add(new Vector3(-sideLength, -sideLength, sideLength)); // 10
                uvs.Add(new Vector2(0f, 1f));

                verts.Add(new Vector3(-sideLength, -sideLength, -sideLength)); // 11
                uvs.Add(new Vector2(1f, 1f));

                tris.Add(index);
                tris.Add(index + 1);
                tris.Add(index + 2);
                tris.Add(index + 2);
                tris.Add(index + 1);
                tris.Add(index + 3);
                index += 4;
            }

            // Right 
            if ((m_sides & CubeSides.Right) != 0)
            {
                verts.Add(new Vector3(sideLength, sideLength, -sideLength)); // 12
                uvs.Add(new Vector2(0f, 0f));

                verts.Add(new Vector3(sideLength, sideLength, sideLength)); // 13
                uvs.Add(new Vector2(1f, 0f));

                verts.Add(new Vector3(sideLength, -sideLength, -sideLength)); // 14
                uvs.Add(new Vector2(0f, 1f));

                verts.Add(new Vector3(sideLength, -sideLength, sideLength)); // 15
                uvs.Add(new Vector2(1f, 1f));

                tris.Add(index);
                tris.Add(index + 1);
                tris.Add(index + 2);
                tris.Add(index + 2);
                tris.Add(index + 1);
                tris.Add(index + 3);
                index += 4;
            }

            // Top
            if ((m_sides & CubeSides.Top) != 0)
            {
                verts.Add(new Vector3(-sideLength, sideLength, sideLength)); // 16
                uvs.Add(new Vector2(0f, 0f));

                verts.Add(new Vector3(sideLength, sideLength, sideLength)); // 17
                uvs.Add(new Vector2(1f, 0f));

                verts.Add(new Vector3(-sideLength, sideLength, -sideLength)); // 18
                uvs.Add(new Vector2(0f, 1f));

                verts.Add(new Vector3(sideLength, sideLength, -sideLength)); // 19
                uvs.Add(new Vector2(1f, 1f));

                tris.Add(index);
                tris.Add(index + 1);
                tris.Add(index + 2);
                tris.Add(index + 2);
                tris.Add(index + 1);
                tris.Add(index + 3);
                index += 4;
            }

            // Bottom
            if ((m_sides & CubeSides.Bottom) != 0)
            {
                verts.Add(new Vector3(-sideLength, -sideLength, -sideLength)); // 20
                uvs.Add(new Vector2(0f, 0f));

                verts.Add(new Vector3(sideLength, -sideLength, -sideLength)); // 21
                uvs.Add(new Vector2(1f, 0f));

                verts.Add(new Vector3(-sideLength, -sideLength, sideLength)); // 22
                uvs.Add(new Vector2(0f, 1f));

                verts.Add(new Vector3(sideLength, -sideLength, sideLength)); // 23
                uvs.Add(new Vector2(1f, 1f));

                tris.Add(index);
                tris.Add(index + 1);
                tris.Add(index + 2);
                tris.Add(index + 2);
                tris.Add(index + 1);
                tris.Add(index + 3);
                index += 4;
            }

            var mesh = new Mesh();
            mesh.vertices = verts.ToArray();
            mesh.triangles = tris.ToArray();
            mesh.uv = uvs.ToArray();
            mesh.RecalculateNormals();

            return mesh;
        }
    }

    [System.Serializable]
    [Flags]
    public enum CubeSides
    {
        None = 0,
        Front = 1,
        Rear = 2,
        Top = 4,
        Bottom = 8,
        Left = 16,
        Right = 32
    }
}