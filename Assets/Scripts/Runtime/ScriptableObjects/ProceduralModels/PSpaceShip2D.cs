using System.Collections.Generic;
using Runtime.Gameplay;
using UnityEngine;

namespace Runtime.ScriptableObjects.ProceduralModels
{
    [CreateAssetMenu(fileName = "SpaceShip2D", menuName = "Procedural/SpaceShip 2D", order = 0)]
    public class PSpaceShip2D : ProceduralModel
    {
        [Header("Ship Parameters")] 
        [Header("Fuselage")]
        [SerializeField] private float m_shipLength = 3f;
        [SerializeField] private float m_frontWidth = 0.5f;
        [SerializeField] private float m_noseLength = 0.1f;
        [SerializeField] private float m_rearWidth = 0.3f;
        [SerializeField] private float m_rearLength = 0.5f;

        [Header("Wings")] 
        [SerializeField] private float m_wingSpan = 2f;
        [SerializeField] private float m_wingPosition = 1f;
        [SerializeField] private float m_frontTipOffset = 0f;
        [SerializeField] private float m_rearTipOffset = 0f;
        [SerializeField] private float m_wingDepth = 0.5f;
        [SerializeField] private float m_frontWingWidth = 0.5f;
        [SerializeField] private float m_rearWingWidth = 0.5f;

        public override Mesh GenerateMesh()
        {
            var verts = new List<Vector3>();
            
            // Define Fuselage
            verts.Add(new Vector3(0f, -m_shipLength * 0.5f, 0f)); // 0 Rear Point
            verts.Add(new Vector3(0f, m_shipLength * 0.5f, 0f)); // 1 Front Point
            verts.Add(new Vector3(-m_frontWidth * 0.5f, m_shipLength * 0.5f - m_noseLength, 0f)); // 2 Left Front
            verts.Add(new Vector3(m_frontWidth * 0.5f, m_shipLength * 0.5f - m_noseLength, 0f)); // 3 Right Front
            verts.Add(new Vector3(-m_rearWidth * 0.5f, -m_shipLength * 0.5f - m_rearLength, 0f)); // 4 Left Back
            verts.Add(new Vector3(m_rearWidth * 0.5f, -m_shipLength * 0.5f - m_rearLength, 0f)); // 5 Right Back
            
            // Define Wings
            verts.Add(new Vector3(-m_wingSpan * 0.5f, m_wingPosition + m_frontTipOffset, 0f)); // 6 Left Front Tip
            verts.Add(new Vector3(-m_wingSpan * 0.5f, m_wingPosition + m_rearTipOffset, 0f)); // 7 Left Rear Tip
            verts.Add(new Vector3(-m_frontWingWidth * 0.5f, m_wingPosition, 0f)); // 8 Left Wing Fuselage Front
            verts.Add(new Vector3(-m_rearWingWidth * 0.5f, m_wingPosition - m_wingDepth, 0f)); // 9 Left Wing Fuselage Rear
            verts.Add(new Vector3(m_wingSpan * 0.5f, m_wingPosition + m_frontTipOffset, 0f)); // 10 Right Front Tip
            verts.Add(new Vector3(m_wingSpan * 0.5f, m_wingPosition + m_rearTipOffset, 0f)); // 11 Rear Right Tip
            verts.Add(new Vector3(m_frontWingWidth * 0.5f, m_wingPosition, 0f)); // 12 Right Wing Fuselage Front
            verts.Add(new Vector3(m_rearWingWidth * 0.5f, m_wingPosition - m_wingDepth, 0f)); // 13 Right Wing Fuselage Rear
            
            var tris = new List<int>();
            
            // Left Fuselage
            tris.Add(0);
            tris.Add(4);
            tris.Add(9);
            tris.Add(0);
            tris.Add(9);
            tris.Add(8);
            tris.Add(0);
            tris.Add(8);
            tris.Add(2);
            tris.Add(0);
            tris.Add(2);
            tris.Add(1);
            
            // Right Fuselage
            tris.Add(0);
            tris.Add(13);
            tris.Add(5);
            tris.Add(0);
            tris.Add(12);
            tris.Add(13);
            tris.Add(0);
            tris.Add(3);
            tris.Add(12);
            tris.Add(0);
            tris.Add(1);
            tris.Add(3);
            
            // Left Wing
            tris.Add(9);
            tris.Add(7);
            tris.Add(6);
            tris.Add(6);
            tris.Add(8);
            tris.Add(9);
            
            // Right Wing
            tris.Add(13);
            tris.Add(12);
            tris.Add(11);
            tris.Add(11);
            tris.Add(12);
            tris.Add(10);
            
            var mesh = new Mesh();
            mesh.vertices = verts.ToArray();
            mesh.triangles = tris.ToArray();
            //mesh.uv
            
            return mesh;
        }

        public override void DrawDebug(Vector3 position)
        {
            base.DrawDebug(position);
            
            // Define Fuselage
            var rearPoint = new Vector3(0f, -m_shipLength * 0.5f, 0f) + position;
            var frontPoint = new Vector3(0f, m_shipLength * 0.5f, 0f) + position;
            var leftFront = new Vector3(-m_frontWidth * 0.5f, m_shipLength * 0.5f - m_noseLength, 0f) + position;
            var rightFront = new Vector3(m_frontWidth * 0.5f, m_shipLength * 0.5f - m_noseLength, 0f) + position;
            var leftBack = new Vector3(-m_rearWidth * 0.5f, -m_shipLength * 0.5f - m_rearLength, 0f) + position;
            var rightBack = new Vector3(m_rearWidth * 0.5f, -m_shipLength * 0.5f - m_rearLength, 0f) + position;
            
            // Define Wings
            var leftFrontTip = new Vector3(-m_wingSpan * 0.5f, m_wingPosition + m_frontTipOffset, 0f) + position;
            var leftRearTip = new Vector3(-m_wingSpan * 0.5f, m_wingPosition + m_rearTipOffset, 0f) + position;
            var leftWingFuselageFront = new Vector3(-m_frontWingWidth * 0.5f, m_wingPosition, 0f) + position;
            var leftWingFuselageRear = new Vector3(-m_rearWingWidth * 0.5f, m_wingPosition - m_wingDepth, 0f) + position;
            var rightFrontTip = new Vector3(m_wingSpan * 0.5f, m_wingPosition + m_frontTipOffset, 0f) + position;
            var rightRearTip = new Vector3(m_wingSpan * 0.5f, m_wingPosition + m_rearTipOffset, 0f) + position;
            var rightWingFuselageFront = new Vector3(m_frontWingWidth * 0.5f, m_wingPosition, 0f) + position;
            var rightWingFuselageRear = new Vector3(m_rearWingWidth * 0.5f, m_wingPosition - m_wingDepth, 0f) + position;
            
            // Draw Fuselage
            Debug.DrawLine(rearPoint, frontPoint, Color.green);
            Debug.DrawLine(frontPoint, leftFront, Color.green);
            Debug.DrawLine(frontPoint, rightFront, Color.green);
            Debug.DrawLine(leftFront, leftWingFuselageFront, Color.green);
            Debug.DrawLine(rightFront, rightWingFuselageFront, Color.green);
            Debug.DrawLine(leftWingFuselageRear, leftBack, Color.green);
            Debug.DrawLine(rightWingFuselageRear, rightBack, Color.green);
            Debug.DrawLine(leftBack, rearPoint, Color.green);
            Debug.DrawLine(rightBack, rearPoint, Color.green);
            
            // Draw Wings
            Debug.DrawLine(leftFrontTip, leftWingFuselageFront, Color.green);
            Debug.DrawLine(leftRearTip, leftWingFuselageRear, Color.green);
            Debug.DrawLine(rightFrontTip, rightWingFuselageFront, Color.green);
            Debug.DrawLine(rightRearTip, rightWingFuselageRear, Color.green);
            Debug.DrawLine(leftWingFuselageFront, leftWingFuselageRear, Color.green);
            Debug.DrawLine(rightWingFuselageFront, rightWingFuselageRear, Color.green);
            Debug.DrawLine(leftFrontTip, leftRearTip, Color.green);
            Debug.DrawLine(rightFrontTip, rightRearTip, Color.green);
        }
    }
}