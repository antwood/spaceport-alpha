using UnityEngine;

namespace Runtime.ScriptableObjects.ProceduralModels
{
    public abstract class ProceduralModel : ScriptableObject
    {
        public Mesh Mesh { get; protected set; }
        
        public Material Material;
        
        public virtual Mesh GenerateMesh()
        {
            Mesh = new Mesh();
            return Mesh;
        }

        public virtual void DrawDebug(Vector3 position)
        {
            
        }
    }
}