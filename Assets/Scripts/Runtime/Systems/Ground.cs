using System;
using System.Collections.Generic;
using System.Linq;
using Runtime.ScriptableObjects.ProceduralModels;
using UnityEngine;

namespace Runtime.Systems
{
    public class Ground : MonoBehaviour
    {
        [SerializeField] private PCube[] m_mapping;

        Dictionary<Vector3, int> m_sidemap;
        
        public void UpdateMappings()
        {
            if (m_sidemap == null)
            {
                m_sidemap = new Dictionary<Vector3, int>();
            }
            
            var children = GetComponentsInChildren<ProceduralModelDrawer>();

            var childPositions = new List<Vector3>();
            
            foreach (var child in children)
            {
                childPositions.Add(child.transform.position);
            }

            var newSides = new List<int>();
            
            foreach (var position in childPositions)
            {
                newSides.Add(CalculateSides(position, ref childPositions));
            }

            for (int i = 0; i < childPositions.Count; i++)
            {
                if (!m_sidemap.ContainsKey(childPositions[i]))
                {
                    m_sidemap.Add(childPositions[i], newSides[i]);
                }
            }

            foreach (var child in children)
            {
                var newModel = m_mapping.FirstOrDefault(m => (int)m.Sides == m_sidemap[child.transform.position]);
                if (newModel != null)
                {
                    child.Model = newModel;
                }
            }
        }

        private int CalculateSides(Vector3 position, ref List<Vector3> boardPositions)
        {
            CubeSides sides = CubeSides.None;
            
            if (boardPositions.Contains(position + new Vector3(0f, 0f, 1f))) sides &= CubeSides.Rear;
            if (boardPositions.Contains(position + new Vector3(0f, 0f, -1f))) sides &= CubeSides.Front;
            if (boardPositions.Contains(position + new Vector3(-1f, 0f, 0f))) sides &= CubeSides.Left;
            if (boardPositions.Contains(position + new Vector3(1f, 0f, 0f))) sides &= CubeSides.Right;
            sides &= CubeSides.Top;

            return (int)sides;
        }
    }

    [System.Serializable]
    public class GroundMapping
    {
        public GroundModel Top;
        public GroundModel Font;
        public GroundModel Right;
        public GroundModel Left;
        public GroundModel FrontLeft;
        public GroundModel FrontRight;
        public GroundModel RearLeft;
        public GroundModel RearRight;
    }
    
    [System.Serializable]
    public class GroundModel
    {
        public PCube Model;
        public CubeSides Sides;
    }
}