using UnityEngine;

namespace Runtime.Systems
{
    public interface IPoolable
    {
        public string GetTag();
        public GameObject GetGameObject();

        public void Reset();
    }
}