using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

namespace Runtime.Systems
{
    public class ObjectPool : MonoBehaviour
    {
        public static ObjectPool Instance { get; private set; }

        private void Awake()
        { 
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
                return;
            }
            
            m_objectPool = new Dictionary<int, List<IPoolable>>();
        }

        private void OnDestroy()
        {
            if (Instance == this)
            {
                Instance = null;
            }
        }

        private Dictionary<int, List<IPoolable>> m_objectPool;
        [SerializeField] private GameObject[] m_prewarmList;

        private void Start()
        {
            PreWarmPool();
        }

        private void PreWarmPool()
        {
            if (m_prewarmList == null) return;
            
            foreach (var poolable in m_prewarmList)
            {
                AddObject(poolable.GetComponent<IPoolable>());
            }
        }

        public int GetObjectHash(string poolTag)
        {
            return poolTag.GetHashCode();
        }

        public int AddObject(IPoolable poolObject)
        {
            Assert.IsNotNull(poolObject, "Pool Object cannot be null");
            
            var instance = Instantiate(poolObject.GetGameObject(), transform);
            instance.SetActive(false);

            instance.name = instance.name.Replace("(Clone)", string.Empty);
            
            var poolable = instance.GetComponent<IPoolable>();
                
            var hash = GetObjectHash(poolable.GetTag());

            if (!m_objectPool.ContainsKey(hash))
            {
                m_objectPool.Add(hash, new List<IPoolable>());
            }
            
            m_objectPool[hash].Add(poolable);

            return hash;
        }
        
        public IPoolable GetObject(int hash)
        {
            IPoolable returnObject = null;
            
            if (m_objectPool.ContainsKey(hash))
            {
                returnObject = m_objectPool[hash].FirstOrDefault(p => !p.GetGameObject().activeSelf);

                if (returnObject == null)
                {
                    // No available objects in the pool. Create a new one based on an existing pool object.
                    returnObject = GetObject(AddObject(m_objectPool[hash][0]));
                }
            }
            else
            {
                throw new HashNotFoundException(hash);
            }

            return returnObject;
        }

        public IPoolable GetObject(int hash, Vector3 position, Quaternion rotation)
        {
            var returnObject = GetObject(hash);
            returnObject.GetGameObject().transform.position = position;
            returnObject.GetGameObject().transform.rotation = rotation;

            return returnObject;
        }
        
        public IPoolable GetObject(int hash, Transform parent, Vector3 position, Quaternion rotation)
        {
            var returnObject = GetObject(hash, position, rotation);
            returnObject.GetGameObject().transform.parent = parent;

            return returnObject;
        }

        public void ReturnObject(IPoolable poolObject)
        {
            poolObject.GetGameObject().transform.parent = transform;
            poolObject.GetGameObject().SetActive(false);
        }
    }

    public class HashNotFoundException : Exception
    {
        public HashNotFoundException(int hash) : base($"Hash {hash} doesn't exist in the object pool.") { }
    }
}