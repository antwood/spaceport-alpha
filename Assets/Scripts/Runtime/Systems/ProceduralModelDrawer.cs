using System;
using Runtime.ScriptableObjects;
using Runtime.ScriptableObjects.ProceduralModels;
using UnityEngine;

namespace Runtime.Systems
{
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    public class ProceduralModelDrawer : MonoBehaviour
    {
        [SerializeField] private ProceduralModel m_model;
        [SerializeField] private Material m_overrideMaterial;
        
        public ProceduralModel Model
        {
            get => m_model;
            set
            {
                m_model = value;
                GenerateModel();
            }
        }
        
        private MeshFilter m_meshFilter;
        private MeshRenderer m_meshRenderer;
        
        private void Awake()
        {
            if (m_meshFilter == null) m_meshFilter = GetComponent<MeshFilter>();
            if (m_meshRenderer == null) m_meshRenderer = GetComponent<MeshRenderer>();
            
            GenerateModel();
        }

        public void GenerateModel()
        {
            if (m_model != null)
            {
                m_meshFilter.mesh = m_model.GenerateMesh();
                m_meshRenderer.material = m_overrideMaterial != null ? m_overrideMaterial : m_model.Material;

                name = m_model.name;
            }
        }

        private void OnValidate()
        {
            if (m_meshFilter == null) m_meshFilter = GetComponent<MeshFilter>();
            if (m_meshRenderer == null) m_meshRenderer = GetComponent<MeshRenderer>();
            
            GenerateModel();
        }

        private void OnDrawGizmos()
        {
            if (m_model != null)
            {
                m_model.DrawDebug(transform.position);
            }
        }
    }
}