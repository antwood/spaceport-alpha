using System;
using Runtime.Gameplay;
using UnityEngine;

namespace Runtime.Systems.ShipGenerator
{
    public class ShipGenerator : MonoBehaviour
    {
        [Header("Ship Parameters")] 
        [Header("Fuselage")]
        [SerializeField] private float m_shipLength = 3f;
        [SerializeField] private float m_frontWidth = 0.5f;
        [SerializeField] private float m_noseLength = 0.1f;
        [SerializeField] private float m_rearWidth = 0.3f;
        [SerializeField] private float m_rearLength = 0.5f;

        [Header("Wings")] 
        [SerializeField] private float m_wingSpan = 2f;
        [SerializeField] private float m_wingPosition = 1f;
        [SerializeField] private float m_frontTipOffset = 0f;
        [SerializeField] private float m_rearTipOffset = 0f;
        [SerializeField] private float m_wingDepth = 0.5f;
        [SerializeField] private float m_frontWingWidth = 0.5f;
        [SerializeField] private float m_rearWingWidth = 0.5f;

        [Header("Contouring")] 
        [Header("Fuselage Top")]
        [SerializeField] private float m_topFuselageXScale = 0.1f;
        [SerializeField] private float m_topFuselageYScale = 0.1f;
        [SerializeField] private float m_topFuselageZScale = 0.1f;
        [SerializeField] private float m_topFuselageXOffset = 0f;
        [SerializeField] private float m_topFuselageYOffset = 0f;
        [SerializeField] private float m_topFuselageZOffset = 0f;
        [SerializeField] private int m_topFuselageContourCount = 20;
        [SerializeField] private float m_topCurve = 0.5f;
        
        [Header("Fuselage Bottom")]
        [SerializeField] private float m_bottomFuselageXScale = 0.1f;
        [SerializeField] private float m_bottomFuselageYScale = 0.1f;
        [SerializeField] private float m_bottomFuselageZScale = 0.1f;
        [SerializeField] private float m_bottomFuselageXOffset = 0f;
        [SerializeField] private float m_bottomFuselageYOffset = 0f;
        [SerializeField] private float m_bottomFuselageZOffset = 0f;
        [SerializeField] private int m_bottomFuselageContourCount = 20;
        [SerializeField] private float m_bottomCurve = 0.5f;
        
        [Header("Child Components")]
        [SerializeField] private MeshFilter m_fuselageMeshFilter;
        [SerializeField] private MeshFilter m_wingsMeshFilter;
        [SerializeField] private MeshFilter m_detailsMeshFilter;

        [SerializeField] private MeshRenderer m_fuselageMeshRenderer;
        [SerializeField] private MeshRenderer m_wingsMeshRenderer;
        [SerializeField] private MeshRenderer m_detailsMeshRenderer;

        private void GenerateShip()
        {
            // Generate Fuselage
            var fuselageMesh = GenerateFuselageMesh();

            // Generate Wings
            var wingsMesh = GenerateWingsMesh();

            // Generate Details
            var detailsMesh = GenerateDetailsMesh();
        }

        private Mesh GenerateDetailsMesh()
        {
            return null;
        }

        private Mesh GenerateWingsMesh()
        {
            return null;
        }

        private Mesh GenerateFuselageMesh()
        {
            return null;
        }

        private void OnDrawGizmos()
        {
            // Define Fuselage
            var rearPoint = new Vector3(0f, -m_shipLength * 0.5f, 0f);
            var frontPoint = new Vector3(0f, m_shipLength * 0.5f, 0f);
            var leftFront = new Vector3(-m_frontWidth * 0.5f, m_shipLength * 0.5f - m_noseLength, 0f);
            var rightFront = new Vector3(m_frontWidth * 0.5f, m_shipLength * 0.5f - m_noseLength, 0f);
            var leftBack = new Vector3(-m_rearWidth * 0.5f, -m_shipLength * 0.5f - m_rearLength, 0f);
            var rightBack = new Vector3(m_rearWidth * 0.5f, -m_shipLength * 0.5f - m_rearLength, 0f);
            
            // Define Wings
            var leftFrontTip = new Vector3(-m_wingSpan * 0.5f, m_wingPosition + m_frontTipOffset, 0f);
            var leftRearTip = new Vector3(-m_wingSpan * 0.5f, m_wingPosition + m_rearTipOffset, 0f);
            var leftWingFuselageFront = new Vector3(-m_frontWingWidth * 0.5f, m_wingPosition, 0f);
            var leftWingFuselageRear = new Vector3(-m_rearWingWidth * 0.5f, m_wingPosition - m_wingDepth, 0f);
            var rightFrontTip = new Vector3(m_wingSpan * 0.5f, m_wingPosition + m_frontTipOffset, 0f);
            var rightRearTip = new Vector3(m_wingSpan * 0.5f, m_wingPosition + m_rearTipOffset, 0f);
            var rightWingFuselageFront = new Vector3(m_frontWingWidth * 0.5f, m_wingPosition, 0f);
            var rightWingFuselageRear = new Vector3(m_rearWingWidth * 0.5f, m_wingPosition - m_wingDepth, 0f);
            
            // Draw Fuselage
            Debug.DrawLine(rearPoint, frontPoint, Color.green);
            Debug.DrawLine(frontPoint, leftFront, Color.green);
            Debug.DrawLine(frontPoint, rightFront, Color.green);
            Debug.DrawLine(leftFront, leftWingFuselageFront, Color.green);
            Debug.DrawLine(rightFront, rightWingFuselageFront, Color.green);
            Debug.DrawLine(leftWingFuselageRear, leftBack, Color.green);
            Debug.DrawLine(rightWingFuselageRear, rightBack, Color.green);
            Debug.DrawLine(leftBack, rearPoint, Color.green);
            Debug.DrawLine(rightBack, rearPoint, Color.green);
            
            // Draw Wings
            Debug.DrawLine(leftFrontTip, leftWingFuselageFront, Color.green);
            Debug.DrawLine(leftRearTip, leftWingFuselageRear, Color.green);
            Debug.DrawLine(rightFrontTip, rightWingFuselageFront, Color.green);
            Debug.DrawLine(rightRearTip, rightWingFuselageRear, Color.green);
            Debug.DrawLine(leftWingFuselageFront, leftWingFuselageRear, Color.green);
            Debug.DrawLine(rightWingFuselageFront, rightWingFuselageRear, Color.green);
            Debug.DrawLine(leftFrontTip, leftRearTip, Color.green);
            Debug.DrawLine(rightFrontTip, rightRearTip, Color.green);
            
            // Fuselage Top Contour
            for (int i = 1; i <= m_topFuselageContourCount; i++)
            {
                var scaleVector = new Vector3(
                    1f - i * m_topFuselageXScale * (i / (float)m_topFuselageContourCount * m_topCurve), 
                    1f - i * m_topFuselageYScale * (i / (float)m_topFuselageContourCount * m_topCurve), 
                    1f - i * m_topFuselageZScale * (i / (float)m_topFuselageContourCount * m_topCurve));
                
                
                
                var inverseScaleVector = new Vector3(i * m_topFuselageXScale, i * m_topFuselageYScale, i * m_topFuselageZScale);
                var offsetVector = new Vector3(m_topFuselageXOffset, m_topFuselageYOffset, m_topFuselageZOffset).VectorScale(inverseScaleVector);
                //Debug.DrawLine(rearPoint.VectorScale(scaleVector).VectorOffset(offsetVector), frontPoint.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                Debug.DrawLine(frontPoint.VectorScale(scaleVector).VectorOffset(offsetVector), leftFront.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                Debug.DrawLine(frontPoint.VectorScale(scaleVector).VectorOffset(offsetVector), rightFront.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                Debug.DrawLine(leftFront.VectorScale(scaleVector).VectorOffset(offsetVector), leftWingFuselageFront.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                Debug.DrawLine(rightFront.VectorScale(scaleVector).VectorOffset(offsetVector), rightWingFuselageFront.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                Debug.DrawLine(leftWingFuselageRear.VectorScale(scaleVector).VectorOffset(offsetVector), leftBack.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                Debug.DrawLine(rightWingFuselageRear.VectorScale(scaleVector).VectorOffset(offsetVector), rightBack.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                Debug.DrawLine(leftBack.VectorScale(scaleVector).VectorOffset(offsetVector), rearPoint.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                Debug.DrawLine(rightBack.VectorScale(scaleVector).VectorOffset(offsetVector), rearPoint.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                Debug.DrawLine(leftWingFuselageFront.VectorScale(scaleVector).VectorOffset(offsetVector), leftWingFuselageRear.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                Debug.DrawLine(rightWingFuselageFront.VectorScale(scaleVector).VectorOffset(offsetVector), rightWingFuselageRear.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                
                // Wings aren't as high.
                if (i < m_topFuselageContourCount / 3)
                {
                    Debug.DrawLine(leftFrontTip.VectorScale(scaleVector).VectorOffset(offsetVector), leftWingFuselageFront.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                    Debug.DrawLine(leftRearTip.VectorScale(scaleVector).VectorOffset(offsetVector), leftWingFuselageRear.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                    Debug.DrawLine(rightFrontTip.VectorScale(scaleVector).VectorOffset(offsetVector), rightWingFuselageFront.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                    Debug.DrawLine(rightRearTip.VectorScale(scaleVector).VectorOffset(offsetVector), rightWingFuselageRear.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                    Debug.DrawLine(leftWingFuselageFront.VectorScale(scaleVector).VectorOffset(offsetVector), leftWingFuselageRear.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                    Debug.DrawLine(rightWingFuselageFront.VectorScale(scaleVector).VectorOffset(offsetVector), rightWingFuselageRear.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                    Debug.DrawLine(leftFrontTip.VectorScale(scaleVector).VectorOffset(offsetVector), leftRearTip.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                    Debug.DrawLine(rightFrontTip.VectorScale(scaleVector).VectorOffset(offsetVector), rightRearTip.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                }
            }
            
            // Fuselage Bottom Contour
            for (int i = 1; i <= m_bottomFuselageContourCount; i++)
            {
                var scaleVector = new Vector3(
                    1f - i * m_bottomFuselageXScale * (i / (float)m_bottomFuselageContourCount * m_bottomCurve), 
                    1f - i * m_bottomFuselageYScale * (i / (float)m_bottomFuselageContourCount * m_bottomCurve), 
                    1f - i * m_bottomFuselageZScale * (i / (float)m_bottomFuselageContourCount * m_bottomCurve));
                
                var inverseScaleVector = new Vector3(i * m_bottomFuselageXScale, i * m_bottomFuselageYScale, i * m_bottomFuselageZScale);
                var offsetVector = new Vector3(m_bottomFuselageXOffset, m_bottomFuselageYOffset, m_bottomFuselageZOffset).VectorScale(inverseScaleVector);
                //Debug.DrawLine(rearPoint.VectorScale(scaleVector).VectorOffset(offsetVector), frontPoint.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                Debug.DrawLine(frontPoint.VectorScale(scaleVector).VectorOffset(offsetVector), leftFront.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                Debug.DrawLine(frontPoint.VectorScale(scaleVector).VectorOffset(offsetVector), rightFront.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                Debug.DrawLine(leftFront.VectorScale(scaleVector).VectorOffset(offsetVector), leftWingFuselageFront.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                Debug.DrawLine(rightFront.VectorScale(scaleVector).VectorOffset(offsetVector), rightWingFuselageFront.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                Debug.DrawLine(leftWingFuselageRear.VectorScale(scaleVector).VectorOffset(offsetVector), leftBack.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                Debug.DrawLine(rightWingFuselageRear.VectorScale(scaleVector).VectorOffset(offsetVector), rightBack.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                Debug.DrawLine(leftBack.VectorScale(scaleVector).VectorOffset(offsetVector), rearPoint.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                Debug.DrawLine(rightBack.VectorScale(scaleVector).VectorOffset(offsetVector), rearPoint.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                Debug.DrawLine(leftWingFuselageFront.VectorScale(scaleVector).VectorOffset(offsetVector), leftWingFuselageRear.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                Debug.DrawLine(rightWingFuselageFront.VectorScale(scaleVector).VectorOffset(offsetVector), rightWingFuselageRear.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                
                // Wings aren't as high.
                if (i < m_topFuselageContourCount / 3)
                {
                    Debug.DrawLine(leftFrontTip.VectorScale(scaleVector).VectorOffset(offsetVector), leftWingFuselageFront.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                    Debug.DrawLine(leftRearTip.VectorScale(scaleVector).VectorOffset(offsetVector), leftWingFuselageRear.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                    Debug.DrawLine(rightFrontTip.VectorScale(scaleVector).VectorOffset(offsetVector), rightWingFuselageFront.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                    Debug.DrawLine(rightRearTip.VectorScale(scaleVector).VectorOffset(offsetVector), rightWingFuselageRear.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                    Debug.DrawLine(leftWingFuselageFront.VectorScale(scaleVector).VectorOffset(offsetVector), leftWingFuselageRear.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                    Debug.DrawLine(rightWingFuselageFront.VectorScale(scaleVector).VectorOffset(offsetVector), rightWingFuselageRear.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                    Debug.DrawLine(leftFrontTip.VectorScale(scaleVector).VectorOffset(offsetVector), leftRearTip.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                    Debug.DrawLine(rightFrontTip.VectorScale(scaleVector).VectorOffset(offsetVector), rightRearTip.VectorScale(scaleVector).VectorOffset(offsetVector), Color.grey);
                }
            }
        }
    }
}