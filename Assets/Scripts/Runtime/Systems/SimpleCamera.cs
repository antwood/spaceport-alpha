using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Runtime.Systems
{
    public class SimpleCamera : MonoBehaviour
    {
        private PlayerInput m_input;
        private float m_moveSpeed = 3f;

        private void Awake()
        {
            m_input = GetComponent<PlayerInput>();
        }

        private void Update()
        {
            var position = transform.position;

            var input = m_input.actions["Move"].ReadValue<Vector2>();

            position += new Vector3(input.x, 0f, input.y) * Time.deltaTime * m_moveSpeed;

            transform.position = position;
        }
    }
}