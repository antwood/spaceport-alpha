using UnityEngine;
using System.Collections.Generic;

namespace Runtime.Systems
{
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    public class StarField : MonoBehaviour
    {
        private MeshFilter m_meshFilter;

        private List<Vector3> m_verts;
        private List<int> m_tris;

        [SerializeField] private int m_starCount = 20;
        [SerializeField] float m_halfWidth = 0.05f;

        [SerializeField] private Bounds m_bounds;

        [SerializeField] private Vector3 m_moveDirection;
        [SerializeField] private float m_moveSpeed = 1f;
        
        private Vector3[] m_vertArray;
        
        void Start()
        {
            m_verts = new List<Vector3>();
            m_tris = new List<int>();

            m_meshFilter = GetComponent<MeshFilter>();

            for (int i = 0; i < m_starCount; i++)
            {
                CreateQuad(i, new Vector3(
                    Random.Range(m_bounds.min.x, m_bounds.max.x), 
                    Random.Range(m_bounds.min.y, m_bounds.max.y), 
                    Random.Range(m_bounds.min.z, m_bounds.max.z)));
            }
            
            var mesh = new Mesh();
            
            mesh.vertices = m_verts.ToArray();
            mesh.triangles = m_tris.ToArray();

            m_meshFilter.mesh = mesh;

            m_vertArray = mesh.vertices;
        }

        // Update is called once per frame
        void Update()
        {
            MoveVerts();

            m_meshFilter.mesh.vertices = m_vertArray;
        }

        private void MoveVerts()
        {
            for (var i = 0; i < m_vertArray.Length; i += 4)
            {
                bool reset = false;
                for (int j = 0; j < 4; j++)
                {
                    m_vertArray[i + j] += m_moveDirection * Time.deltaTime * m_moveSpeed;

                    if (i + j > 1)
                    {
                        if (m_vertArray[i + j].y < m_bounds.min.y)
                        {
                            reset = true;
                        }
                    }
                }

                if (reset)
                {
                    m_vertArray[i].y += m_bounds.extents.y * 2;
                    m_vertArray[i + 1].y += m_bounds.extents.y * 2;
                    m_vertArray[i + 2].y += m_bounds.extents.y * 2;
                    m_vertArray[i + 3].y += m_bounds.extents.y * 2;
                }
                
                /*m_vertArray[i].y += Time.deltaTime;
                if (m_vertArray[i].y > 20f)
                {
                    m_vertArray[i].z = 10f;
                }*/
            }
        }
        
        private void CreateQuad(int index, Vector3 position)
        {
            m_verts.Add(new Vector3(-m_halfWidth, m_halfWidth, 0f) + position);
            m_verts.Add(new Vector3(m_halfWidth, m_halfWidth, 0f) + position);
            m_verts.Add(new Vector3(-m_halfWidth, -m_halfWidth, 0f) + position);
            m_verts.Add(new Vector3(m_halfWidth, -m_halfWidth, 0f) + position);

            var firstPoint = index * 4;

            m_tris.Add(firstPoint);
            m_tris.Add(firstPoint + 1);
            m_tris.Add(firstPoint + 2);
            m_tris.Add(firstPoint + 2);
            m_tris.Add(firstPoint + 1);
            m_tris.Add(firstPoint + 3);
        }
    }
}