using System;
using System.Linq;
using Runtime.ScriptableObjects;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;

namespace Runtime.UI
{
    public class DialoguePanel : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI m_characterLabel;
        [SerializeField] private TextMeshProUGUI m_dialogueLabel;

        [SerializeField] private Dialogue[] m_dialogues;
        
        private Dialogue m_currentDialogue;
        private int m_currentIndex = 0;

        private Action m_currentCompleteCallback;
        
        public void StartDialogue(string dialogueName, Action completeCallback)
        {
            var dialogue = m_dialogues.FirstOrDefault(d => d.name.Equals(dialogueName));
            
            Assert.IsNotNull(dialogue);

            m_currentIndex = 0;
            m_currentCompleteCallback = completeCallback;
            m_currentDialogue = dialogue;
            gameObject.SetActive(true);
            
            UpdateLabels();
        }

        public void AdvanceDialogue()
        {
            m_currentIndex++;
            
            if (m_currentIndex >= m_currentDialogue.DialoguesElements.Length)
            {
                EndDialogue();
                return;
            }
            
            UpdateLabels();
        }

        public void UpdateLabels()
        {
            m_characterLabel.text = m_currentDialogue.DialoguesElements[m_currentIndex].Name;
            m_dialogueLabel.text = m_currentDialogue.DialoguesElements[m_currentIndex].Dialogue;
        }

        public void EndDialogue()
        {
            gameObject.SetActive(false);
            m_currentCompleteCallback?.Invoke();
        }
    }
}