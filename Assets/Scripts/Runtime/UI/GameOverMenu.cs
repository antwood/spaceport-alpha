using System.Collections;
using Runtime.Gameplay;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Runtime.UI
{
    public class GameOverMenu : Menu
    {
        [SerializeField] private Image m_background;
        
        public override void Show()
        {
            m_background.gameObject.SetActive(true);
            
            SetMenuSet("gameover");
            base.Show();
        }

        public void Restart()
        {
            StartCoroutine(DoRestart());
        }
        
        public void Quit()
        {
            StartCoroutine(DoQuitGame());
        }
        
        private IEnumerator DoQuitGame()
        {
            LoadScreen.Instance.Show();
            
            yield return new WaitForSeconds(1f);
            
            SceneManager.LoadSceneAsync("Menu");
        }
        
        private IEnumerator DoRestart()
        {
            LoadScreen.Instance.Show();
            
            yield return new WaitForSeconds(1f);
            
            SceneManager.LoadSceneAsync("Game");
        }
    }
}