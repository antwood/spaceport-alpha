using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using TMPro;
using UnityEngine;
using Multiplatform.Common;

namespace Runtime.UI
{
    public class HighScoreTable : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI m_names;
        [SerializeField] private TextMeshProUGUI m_scores;

        private void OnEnable()
        {
            PlatformManager.Instance.LoadData("high_scores", OnDataLoaded);
        }

        private void OnDataLoaded(byte[] data)
        {
            if (data != null)
            {
                using (var ms = new MemoryStream(data))
                {
                    var bf = new BinaryFormatter();

                    var table = bf.Deserialize(ms) as Dictionary<string, int>;

                    var namesBuilder = new StringBuilder();
                    var scoresBuilder = new StringBuilder();
                    
                    foreach (var kvp in table)
                    {
                        namesBuilder.AppendLine(kvp.Key);
                        scoresBuilder.AppendLine(kvp.Value.ToString("###,###,###"));
                    }

                    m_names.text = namesBuilder.ToString();
                    m_scores.text = scoresBuilder.ToString();
                }
            }
        }
    }
}