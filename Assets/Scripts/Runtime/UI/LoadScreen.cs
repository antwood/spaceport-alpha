using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Runtime.UI
{
    public class LoadScreen : MonoBehaviour
    {
        public static LoadScreen Instance { get; private set; }

        [SerializeField] private Image m_loadingIndicator;
        [SerializeField] private float m_fadeTime = 1f;
        
        private CanvasGroup m_canvasGroup;
        
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
                return;
            }

            m_canvasGroup = GetComponent<CanvasGroup>();
            m_canvasGroup.alpha = 0f;

            m_loadingIndicator.color = Color.clear;
            
            gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            if (Instance == this)
            {
                Instance = null;
            }
        }

        public void Show()
        {
            gameObject.SetActive(true);
            StartCoroutine(DoShow());
        }

        private IEnumerator DoShow()
        {
            while (m_canvasGroup.alpha < 1f)
            {
                m_canvasGroup.alpha += Time.deltaTime / m_fadeTime;
                yield return null;
            }

            yield return new WaitForSeconds(m_fadeTime);
            
            var indicatorAlpha = 0f;

            while (indicatorAlpha < 1f)
            {
                indicatorAlpha += Time.deltaTime / m_fadeTime;
                m_loadingIndicator.color = Color.Lerp(Color.clear, Color.white, indicatorAlpha);
                yield return null;
            }
        }
        
        public void Hide()
        {
            StartCoroutine(DoHide());
        }

        private IEnumerator DoHide()
        {
            var indicatorAlpha = 0f;

            while (indicatorAlpha < 1f)
            {
                indicatorAlpha += Time.deltaTime / m_fadeTime;
                m_loadingIndicator.color = Color.Lerp(Color.white, Color.clear, indicatorAlpha);
                yield return null;
            }
            
            yield return new WaitForSeconds(m_fadeTime);
            
            while (m_canvasGroup.alpha > 0f)
            {
                m_canvasGroup.alpha -= Time.deltaTime / m_fadeTime;
                yield return null;
            }
            
            gameObject.SetActive(false);
        }

        private void Update()
        {
            m_loadingIndicator.transform.Rotate(Vector3.forward, Time.deltaTime * 100f);
        }
    }
}