using System.Collections;
using Runtime.ScriptableObjects;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Runtime.UI
{
    public class LogoCycler : MonoBehaviour
    {
        [SerializeField] private LogoSet m_logoSet;
        
        private CanvasGroup m_canvasGroup;
        private Image m_image;

        private void Awake()
        {
            m_canvasGroup = GetComponent<CanvasGroup>();
            m_canvasGroup.alpha = 0f;

            m_image = GetComponentInChildren<Image>();
        }

        private void Start()
        {
            StartCoroutine(DoLogoCycle(m_logoSet));
        }

        private IEnumerator DoLogoCycle(LogoSet logoSet)
        {
            if (logoSet == null)
            {
                yield return null;
            }
            else
            {
                foreach (var sprite in logoSet.Logos)
                {
                    yield return new WaitForSeconds(logoSet.DisplayTime);
                    
                    m_image.sprite = sprite;
                    m_image.preserveAspect = true;

                    while (m_canvasGroup.alpha < 1f)
                    {
                        m_canvasGroup.alpha += Time.deltaTime / logoSet.FadeTime;
                        yield return null;
                    }

                    yield return new WaitForSeconds(logoSet.DisplayTime);

                    while (m_canvasGroup.alpha > 0f)
                    {
                        m_canvasGroup.alpha -= Time.deltaTime / logoSet.FadeTime;
                        yield return null;
                    }
                }
            }

            LoadScreen.Instance.Show();

            yield return new WaitForSeconds(2f);
            
            SceneManager.LoadSceneAsync("Menu");
        }
    }
}