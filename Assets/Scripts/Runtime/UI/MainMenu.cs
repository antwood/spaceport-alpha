using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Runtime.UI
{
    public class MainMenu : Menu
    {
        [SerializeField] private GameObject m_title;
        public override void Show()
        {
            m_title.SetActive(true);
            SetMenuSet("new");
            base.Show();
        }

        public override void Hide()
        {
            m_title.SetActive(false);
            base.Hide();
        }

        public void StartGame()
        {
            StartCoroutine(DoStartGame());
        }

        public void ContinueGame()
        {
            StartGame();
        }

        private IEnumerator DoStartGame()
        {
            LoadScreen.Instance.Show();
            
            yield return new WaitForSeconds(1f);
            
            SceneManager.LoadSceneAsync("Game");
        }
    }
}