using System;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Runtime.UI
{
    public class Menu : MonoBehaviour
    {
        [SerializeField] private Transform m_menuRoot;
        [SerializeField] private MenuSet[] m_menuSets;
        
        public void SetMenuSet(string setName)
        {
            var menuSet = m_menuSets.FirstOrDefault(s => s.Name.Equals(setName));

            if (menuSet != null)
            {
                HideAllItems();

                foreach (var item in menuSet.Items)
                {
                    item.SetActive(true);
                }
                
                EventSystem.current.SetSelectedGameObject(menuSet.Items[0]);
            }
        }

        private void HideAllItems()
        {
            if (m_menuRoot == null) m_menuRoot = transform;
            
            foreach (Transform child in m_menuRoot)
            {
                child.gameObject.SetActive(false);
            }
        }

        public virtual void Show()
        {
            gameObject.SetActive(true);
        }
        
        public virtual void Hide()
        {
            gameObject.SetActive(false);
        }
    }

    [Serializable]
    public class MenuSet
    {
        public string Name;
        public GameObject[] Items;
    }
}