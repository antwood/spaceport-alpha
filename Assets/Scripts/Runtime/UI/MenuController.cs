using System.Linq;
using UnityEngine;

namespace Runtime.UI
{
    public class MenuController : MonoBehaviour
    {
        private Menu[] m_menus;
        private Menu m_currentMenu;

        [SerializeField] private string m_initialMenuName;
        
        private void Awake()
        {
            m_menus = GetComponentsInChildren<Menu>(true);
        }

        private void Start()
        {
            ShowMenu(m_initialMenuName);
            
            if (LoadScreen.Instance) LoadScreen.Instance.Hide();
        }

        public void ShowMenu(string menuName)
        {
            if (menuName.Equals("Clear"))
            {
                foreach (var m in m_menus)
                {
                    m.Hide();
                }

                m_currentMenu = null;
                return;
            }
            
            var menu = m_menus.FirstOrDefault(m => m.name.Equals(menuName));

            if (menu != null)
            {
                if (m_currentMenu != null)
                {
                    m_currentMenu.Hide();
                }

                m_currentMenu = menu;
                m_currentMenu.Show();
            }
        }
    }
}