using System;
using System.Security.Cryptography;
using TMPro;
using UnityEngine;

namespace Runtime.UI
{
    public class MessageLabel : MonoBehaviour
    {
        public static MessageLabel Instance { get; private set; }
        
        private TextMeshProUGUI m_messageLabel;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                m_messageLabel = GetComponent<TextMeshProUGUI>();
            }
            else
            {
                Destroy(gameObject);
            }
            
        }

        private void OnDestroy()
        {
            if (Instance == this)
            {
                Instance = null;
            }
        }

        public void SetMessage(string message, float displayTime)
        {
            CancelInvoke();
            m_messageLabel.text = message;
            
            Invoke(nameof(ClearMessage), displayTime);
        }

        public void ClearMessage()
        {
            m_messageLabel.text = string.Empty;
        }
    }
}