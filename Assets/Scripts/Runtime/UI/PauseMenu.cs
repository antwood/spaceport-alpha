using System.Collections;
using Runtime.Gameplay;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Runtime.UI
{
    public class PauseMenu : Menu
    {
        [SerializeField] private Image m_background;
        
        public override void Show()
        {
            m_background.gameObject.SetActive(true);
            
            SetMenuSet("paused");
            base.Show();
        }

        public override void Hide()
        {
            m_background.gameObject.SetActive(false);
            
            base.Hide();
        }

        public void Resume()
        {
            GameManager.Instance.UnPause();
        }
        
        public void Quit()
        {
            StartCoroutine(DoQuitGame());
        }
        
        private IEnumerator DoQuitGame()
        {
            LoadScreen.Instance.Show();
            
            yield return new WaitForSeconds(1f);
            
            SceneManager.LoadSceneAsync("Menu");
        }
    }
}