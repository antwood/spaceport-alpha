using System;
using TMPro;
using UnityEngine;

namespace Runtime.UI
{
    public class ScoreLabel : MonoBehaviour
    {
        private TextMeshProUGUI m_scoreLabel;

        private void Awake()
        {
            m_scoreLabel = GetComponent<TextMeshProUGUI>();
        }

        public void SetScore(int score)
        {
            m_scoreLabel.text = score.ToString();
        }
    }
}