using System;
using UnityEngine;
using UnityEngine.UI;

namespace Runtime.UI
{
    public class ShieldIndicator : MonoBehaviour
    {
        private Slider m_slider;
        private RectTransform m_rectTransform;
        
        public float Value
        {
            get => m_slider.value;
            set => m_slider.value = value;
        }

        public int Total
        {
            set
            {
                var sizeDelta = m_rectTransform.sizeDelta;
                sizeDelta.x = value * 50f;
                m_rectTransform.sizeDelta = sizeDelta;
            }
        }

        private void Awake()
        {
            m_slider = GetComponentInChildren<Slider>();
            m_rectTransform = GetComponent<RectTransform>();
        }
        
        
    }
}